<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>M3118073 - Ramadhan Wahyu Indra Pradana - Codeigniter Form Helper</title>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>
<body>

<div class="container" style="margin-top: 25px;">
  <?php echo form_open_multipart('welcome/result'); ?>
    <?php echo form_hidden('waktu_input', date('Y-m-d H:i:s')); ?>

    <?php echo validation_errors('<div class="alert alert-danger" role="alert">', '</div>'); ?>

    <h2>Data Mahasiswa</h2>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <?php 
            echo form_label('Nama Lengkap', 'nama_lengkap');
            echo form_input(
              'nama_lengkap', 
              set_value('nama_lengkap'), 
              [
                'name' => 'nama_lengkap', 
                'id' => 'nama_lengkap',
                'class' => 'form-control',
                'placeholder' => "Masukkan Nama Lengkap"
              ]
            );
          ?>          
        </div>
      </div>
      <!--  col-md-6   -->

      <div class="col-md-6">

        <div class="form-group">
          <?php 
            echo form_label('NIM Mahasiswa', 'nim');
            echo form_input(
              'nim', 
              set_value('nim'), 
              [
                'name' => 'nim', 
                'id' => 'nim',
                'class' => 'form-control',
                'placeholder' => "Masukkan NIM Mahasiswa"
              ]
            );
          ?>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <?php 
            echo form_label('Jenis Kelamin', 'jenis_kelamin')."<br>";
            echo form_radio('jenis_kelamin', 'Laki - Laki', 'checked', 'id=jenis_kelamin1').form_label('Laki - Laki', 'jenis_kelamin1')."<br>";
            echo form_radio('jenis_kelamin', 'Perempuan', false, 'id=jenis_kelamin2').form_label('Perempuan', 'jenis_kelamin2');
          ?>
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
          <?php 
            echo form_label('Hobi', null)."<br>";
            echo form_checkbox('hobi[]', 'memancing', set_value('hobi[]'), 'id=hobi1').form_label('Memancing', 'hobi1')."<br>";
            echo form_checkbox('hobi[]', 'olahraga', set_value('hobi[]'), 'id=hobi2').form_label('Olahraga', 'hobi2')."<br>";
            echo form_checkbox('hobi[]', 'travelling', set_value('hobi[]'), 'id=hobi3').form_label('Travelling', 'hobi3')."<br>";
            echo form_checkbox('hobi[]', 'mendaki', set_value('hobi[]'), 'id=hobi4').form_label('Mendaki', 'hobi4')."<br>";
          ?>
        </div>
      </div>

      <!--  col-md-6   -->
    </div>
    <!--  row   -->


    <div class="row">
      <div class="col-md-6">

        <div class="form-group">
          <?php 
            echo form_label('Kelas', 'kelas');
            echo form_dropdown(
              'kelas', 
              [
                'TI A' => 'TI A',
                'TI B' => 'TI B',
                'TI C' => 'TI C',
                'TI D' => 'TI D'
              ],
              set_value('kelas'),
              [
                'name' => 'kelas', 
                'id' => 'kelas',
                'class' => 'form-control',
              ]
            );
          ?>
        </div>
      </div>
      <!--  col-md-6   -->

      

      <div class="col-md-6">
        <div class="form-group">
        <?php 
            echo form_label('Tanggal Lahir', 'tgl_lahir');
            echo form_input(
              'tgl_lahir', 
              set_value('tgl_lahir'),
              [
                'name' => 'tgl_lahir', 
                'id' => 'tgl_lahir',
                'class' => 'form-control',
                'placeholder' => 'Masukkan Tanggal Lahir'
              ]
            );
          ?>
        </div>

      </div>
    
      <div class="col-md-12">
        <div class="form-group">
          <?php 
            echo form_label('Kota Asal', 'kota_asal');
            echo form_dropdown(
                'kota_asal', 
                $kota,
                set_value('kota_asal'),
                [
                  'id' => 'kelas',
                  'class' => 'form-control',
                ]
              );
          ?>
        </div>

      </div>

      <div class="col-md-6">
        <div class="form-group">
          <?php 
            echo form_label('Password', 'password');
            echo form_password(
              'password', 
              null, 
              [
                'id' => 'password',
                'class' => 'form-control',
                'placeholder' => "Masukkan Password"
              ]
            );
          ?>
        </div>

      </div>

      <div class="col-md-6">
        <div class="form-group">
          <?php 
            echo form_label('Konfirmasi Password', 'konfirmasi_password');
            echo form_password(
              'konfirmasi_password', 
              null, 
              [
                'id' => 'konfirmasi_password',
                'class' => 'form-control',
                'placeholder' => "Konfirmasi Password"
              ]
            );
          ?>
        </div>

      </div>

      <!--  col-md-6   -->
    </div>
    <!--  row   -->

    <div class="row">

      <div class="col-md-12">
        <div class="form-group">
          <?php 
            echo form_label('Alamat', 'alamat');
            echo form_textarea(
              'alamat', 
              set_value('alamat'),
              [
                'id' => 'alamat',
                'class' => 'form-control',
                'placeholder' => "Masukkan Alamat"
              ]
            );
          ?>
        </div>


        <div class="form-group">
          <?php 
            echo form_label('Foto', 'foto');
            echo form_upload(
              'foto', 
              null, 
              [
                'id' => 'foto',
              ]
            );
          ?>
        </div>


    <?php echo form_submit(
      'simpan', 
      'Simpan Data', 
      [
        'class' => 'btn btn-primary',
        'type' => 'submit'
      ]);
    ?>            
  <?php echo form_close(); ?>
</div>	

<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#tgl_lahir" ).datepicker();
  } );
  </script>

</body>
</html>