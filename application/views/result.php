<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
</head>
<body>

    <h2>Data Mahasiswa</h2>

    <p>Nama : <?php echo $nama_mahasiswa; ?></p>
    <p>NIM : <?php echo $nim_mahasiswa; ?></p>
    <p>Jenis Kelamin : <?php echo $jenis_kelamin; ?></p>
    <p>Hobi : <?php echo $hobi; ?></p>
    <p>Kelas : <?php echo $kelas; ?></p>
    <p>Kota Asal : <?php echo $kota_asal; ?></p>
    <p>Tanggal Lahir : <?php echo $tanggal_lahir; ?></p>
    <p>Password : <?php echo $password; ?></p>
    <p>Alamat : <?php echo $alamat; ?></p>
    <p>Tanggal Penginputan : <?php echo $tgl_input; ?></p>
    <p>Foto</p>
    <img src="<?php echo base_url($foto_path);?>" alt="" style="max-width:150px">
</body>
</html>