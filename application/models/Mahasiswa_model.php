<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa_model extends CI_Model {

    public function get_kota()
    {
        $kota = $this->db->order_by('nama_kota', 'asc')->get('kota')->result();
        foreach ($kota as $key) {
            $result[$key->id_kota] = $key->nama_kota;
        }

        return $result;
    }

    public function get_kota_by_id($id = 0)
    {
        return $this->db->where('id_kota', $id)->get('kota')->row()->nama_kota;
    }

    public function tambah_mahasiswa($data)
    {
        $this->db->insert('mahasiswa', $data);
    }

}

/* End of file Mahasiswa_model.php */


?>