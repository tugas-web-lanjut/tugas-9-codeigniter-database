<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		date_default_timezone_set('Asia/Jakarta');
		$data = [
			'kota' => $this->mahasiswa_model->get_kota()
		];
		$this->load->view('index', $data, FALSE);
	}

	private function _uploadImage($name, $path)
    {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $_FILES[$name]['name'] = date('YmdHis').'-'.$_FILES[$name]['name'];
        $config = array(
            'upload_path'   => "$path",
            'allowed_types' => 'gif|jpg|png|jpeg',
            'encrypt_name'  => true
        );
        $this->upload->initialize($config);
        if ($this->upload->do_upload($name)) {
            return $data_img = [
                'name' => $this->upload->data("file_name"),
                'path' => $path.'/'.$this->upload->data("file_name"),
            ];
        }else{
            return "default.png";
        }
	}
	
	public function result()
	{
		$tgl_input = $this->input->post('waktu_input');
		$nama_lengkap = $this->input->post('nama_lengkap');
		$nim = $this->input->post('nim');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$hobi = $this->input->post('hobi');
		$kelas = $this->input->post('kelas');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$password = $this->input->post('password');
		$alamat = $this->input->post('alamat');
		$id_kota = $this->input->post('kota_asal');
		$foto = 'foto';

		// Validation begin
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required',
			array('required' => 'Nama harus diisi.')
		);

		$this->form_validation->set_rules('nim', 'NIM', 'required',
			array('required' => 'NIM harus diisi.')
		);

		$this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required',
			array('required' => 'Tanggal Lahir harus diisi.')
		);

		$this->form_validation->set_rules('password', 'Password', 'required',
			array('required' => 'Password harus diisi.')
		);

		$this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi Password', 'required|matches[password]',
			array(
				'required' => 'Konfirmasi Password harus diisi.',
				'matches' => 'Password tidak sama.',
			)
		);

		$this->form_validation->set_rules('alamat', 'Alamat', 'required',
			array('required' => 'Alamat harus diisi.')
		);

		if (empty($_FILES[$foto]['name'])){
			$this->form_validation->set_rules($foto, 'Foto', 'required',
				array('required' => 'Foto harus diisi.')
			);
		}

		if ($this->form_validation->run() == FALSE)
		{
			self::index();
		}
		else
		{
			$image = self::_uploadImage($foto, 'upload/mahasiswa');
			$data = [
				'tgl_input' => $tgl_input,
				'nama_mahasiswa' => $nama_lengkap,
				'nim_mahasiswa' => $nim,
				'jenis_kelamin' => $jenis_kelamin,
				'hobi' => implode("," , $hobi),
				'kelas' => $kelas,
				'tanggal_lahir' => $tgl_lahir,
				'password' => md5($password),
				'alamat' => $alamat,
				'id_kota' => $id_kota,
				'foto' => $image['name'],
				'foto_path' => $image['path'],
			];

			$this->mahasiswa_model->tambah_mahasiswa($data);
			$data['kota_asal'] = $this->mahasiswa_model->get_kota_by_id($id_kota);
			$this->load->view('result', $data, FALSE);
		}
	}
}
