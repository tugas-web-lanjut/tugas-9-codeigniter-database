/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : pemweb_prak9

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 04/05/2020 07:06:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for kota
-- ----------------------------
DROP TABLE IF EXISTS `kota`;
CREATE TABLE `kota`  (
  `id_kota` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kota` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_kota`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 120 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kota
-- ----------------------------
INSERT INTO `kota` VALUES (1, 'Kabupaten Lebak');
INSERT INTO `kota` VALUES (2, 'Kabupaten Pandeglang');
INSERT INTO `kota` VALUES (3, 'Kabupaten Serang');
INSERT INTO `kota` VALUES (4, 'Kabupaten Tangerang');
INSERT INTO `kota` VALUES (5, 'Kota Cilegon');
INSERT INTO `kota` VALUES (6, 'Kota Serang');
INSERT INTO `kota` VALUES (7, 'Kota Tangerang');
INSERT INTO `kota` VALUES (8, 'Kota Tangerang Selatan');
INSERT INTO `kota` VALUES (9, 'Kabupaten Bandung');
INSERT INTO `kota` VALUES (10, 'Kabupaten Bandung Barat');
INSERT INTO `kota` VALUES (11, 'Kabupaten Bekasi');
INSERT INTO `kota` VALUES (12, 'Kabupaten Bogor');
INSERT INTO `kota` VALUES (13, 'Kabupaten Ciamis');
INSERT INTO `kota` VALUES (14, 'Kabupaten Cianjur');
INSERT INTO `kota` VALUES (15, 'Kabupaten Cirebon');
INSERT INTO `kota` VALUES (16, 'Kabupaten Garut');
INSERT INTO `kota` VALUES (17, 'Kabupaten Indramayu');
INSERT INTO `kota` VALUES (18, 'Kabupaten Karawang');
INSERT INTO `kota` VALUES (19, 'Kabupaten Kuningan');
INSERT INTO `kota` VALUES (20, 'Kabupaten Majalengka');
INSERT INTO `kota` VALUES (21, 'Kabupaten Pangandaran');
INSERT INTO `kota` VALUES (22, 'Kabupaten Purwakarta');
INSERT INTO `kota` VALUES (23, 'Kabupaten Subang');
INSERT INTO `kota` VALUES (24, 'Kabupaten Sukabumi');
INSERT INTO `kota` VALUES (25, 'Kabupaten Sumedang');
INSERT INTO `kota` VALUES (26, 'Kabupaten Tasikmalaya');
INSERT INTO `kota` VALUES (27, 'Kota Bandung');
INSERT INTO `kota` VALUES (28, 'Kota Banjar');
INSERT INTO `kota` VALUES (29, 'Kota Bekasi');
INSERT INTO `kota` VALUES (30, 'Kota Bogor');
INSERT INTO `kota` VALUES (31, 'Kota Cimahi');
INSERT INTO `kota` VALUES (32, 'Kota Cirebon');
INSERT INTO `kota` VALUES (33, 'Kota Depok');
INSERT INTO `kota` VALUES (34, 'Kota Sukabumi');
INSERT INTO `kota` VALUES (35, 'Kota Tasikmalaya');
INSERT INTO `kota` VALUES (36, 'Kabupaten Administrasi Kepulauan Seribu');
INSERT INTO `kota` VALUES (37, 'Kota Administrasi Jakarta Barat');
INSERT INTO `kota` VALUES (38, 'Kota Administrasi Jakarta Pusat');
INSERT INTO `kota` VALUES (39, 'Kota Administrasi Jakarta Selatan');
INSERT INTO `kota` VALUES (40, 'Kota Administrasi Jakarta Timur');
INSERT INTO `kota` VALUES (41, 'Kota Administrasi Jakarta Utara');
INSERT INTO `kota` VALUES (42, 'Kabupaten Banjarnegara');
INSERT INTO `kota` VALUES (43, 'Kabupaten Banyumas');
INSERT INTO `kota` VALUES (44, 'Kabupaten Batang');
INSERT INTO `kota` VALUES (45, 'Kabupaten Blora');
INSERT INTO `kota` VALUES (46, 'Kabupaten Boyolali');
INSERT INTO `kota` VALUES (47, 'Kabupaten Brebes');
INSERT INTO `kota` VALUES (48, 'Kabupaten Cilacap');
INSERT INTO `kota` VALUES (49, 'Kabupaten Demak');
INSERT INTO `kota` VALUES (50, 'Kabupaten Grobogan');
INSERT INTO `kota` VALUES (51, 'Kabupaten Jepara');
INSERT INTO `kota` VALUES (52, 'Kabupaten Karanganyar');
INSERT INTO `kota` VALUES (53, 'Kabupaten Kebumen');
INSERT INTO `kota` VALUES (54, 'Kabupaten Kendal');
INSERT INTO `kota` VALUES (55, 'Kabupaten Klaten');
INSERT INTO `kota` VALUES (56, 'Kabupaten Kudus');
INSERT INTO `kota` VALUES (57, 'Kabupaten Magelang');
INSERT INTO `kota` VALUES (58, 'Kabupaten Pati');
INSERT INTO `kota` VALUES (59, 'Kabupaten Pekalongan');
INSERT INTO `kota` VALUES (60, 'Kabupaten Pemalang');
INSERT INTO `kota` VALUES (61, 'Kabupaten Purbalingga');
INSERT INTO `kota` VALUES (62, 'Kabupaten Purworejo');
INSERT INTO `kota` VALUES (63, 'Kabupaten Rembang');
INSERT INTO `kota` VALUES (64, 'Kabupaten Semarang');
INSERT INTO `kota` VALUES (65, 'Kabupaten Sragen');
INSERT INTO `kota` VALUES (66, 'Kabupaten Sukoharjo');
INSERT INTO `kota` VALUES (67, 'Kabupaten Tegal');
INSERT INTO `kota` VALUES (68, 'Kabupaten Temanggung');
INSERT INTO `kota` VALUES (69, 'Kabupaten Wonogiri');
INSERT INTO `kota` VALUES (70, 'Kabupaten Wonosobo');
INSERT INTO `kota` VALUES (71, 'Kota Magelang');
INSERT INTO `kota` VALUES (72, 'Kota Pekalongan');
INSERT INTO `kota` VALUES (73, 'Kota Salatiga');
INSERT INTO `kota` VALUES (74, 'Kota Semarang');
INSERT INTO `kota` VALUES (75, 'Kota Surakarta');
INSERT INTO `kota` VALUES (76, 'Kota Tegal');
INSERT INTO `kota` VALUES (77, 'Kabupaten Bantul');
INSERT INTO `kota` VALUES (78, 'Kabupaten Gunungkidul');
INSERT INTO `kota` VALUES (79, 'Kabupaten Kulon Progo');
INSERT INTO `kota` VALUES (80, 'Kabupaten Sleman');
INSERT INTO `kota` VALUES (81, 'Kota Yogyakarta');
INSERT INTO `kota` VALUES (82, 'Kabupaten Bangkalan');
INSERT INTO `kota` VALUES (83, 'Kabupaten Banyuwangi');
INSERT INTO `kota` VALUES (84, 'Kabupaten Blitar');
INSERT INTO `kota` VALUES (85, 'Kabupaten Bojonegoro');
INSERT INTO `kota` VALUES (86, 'Kabupaten Bondowoso');
INSERT INTO `kota` VALUES (87, 'Kabupaten Gresik');
INSERT INTO `kota` VALUES (88, 'Kabupaten Jember');
INSERT INTO `kota` VALUES (89, 'Kabupaten Jombang');
INSERT INTO `kota` VALUES (90, 'Kabupaten Kediri');
INSERT INTO `kota` VALUES (91, 'Kabupaten Lamongan');
INSERT INTO `kota` VALUES (92, 'Kabupaten Lumajang');
INSERT INTO `kota` VALUES (93, 'Kabupaten Madiun');
INSERT INTO `kota` VALUES (94, 'Kabupaten Magetan');
INSERT INTO `kota` VALUES (95, 'Kabupaten Malang');
INSERT INTO `kota` VALUES (96, 'Kabupaten Mojokerto');
INSERT INTO `kota` VALUES (97, 'Kabupaten Nganjuk');
INSERT INTO `kota` VALUES (98, 'Kabupaten Ngawi');
INSERT INTO `kota` VALUES (99, 'Kabupaten Pacitan');
INSERT INTO `kota` VALUES (100, 'Kabupaten Pamekasan');
INSERT INTO `kota` VALUES (101, 'Kabupaten Pasuruan');
INSERT INTO `kota` VALUES (102, 'Kabupaten Ponorogo');
INSERT INTO `kota` VALUES (103, 'Kabupaten Probolinggo');
INSERT INTO `kota` VALUES (104, 'Kabupaten Sampang');
INSERT INTO `kota` VALUES (105, 'Kabupaten Sidoarjo');
INSERT INTO `kota` VALUES (106, 'Kabupaten Situbondo');
INSERT INTO `kota` VALUES (107, 'Kabupaten Sumenep');
INSERT INTO `kota` VALUES (108, 'Kabupaten Trenggalek');
INSERT INTO `kota` VALUES (109, 'Kabupaten Tuban');
INSERT INTO `kota` VALUES (110, 'Kabupaten Tulungagung');
INSERT INTO `kota` VALUES (111, 'Kota Batu');
INSERT INTO `kota` VALUES (112, 'Kota Blitar');
INSERT INTO `kota` VALUES (113, 'Kota Kediri');
INSERT INTO `kota` VALUES (114, 'Kota Madiun');
INSERT INTO `kota` VALUES (115, 'Kota Malang');
INSERT INTO `kota` VALUES (116, 'Kota Mojokerto');
INSERT INTO `kota` VALUES (117, 'Kota Pasuruan');
INSERT INTO `kota` VALUES (118, 'Kota Probolinggo');
INSERT INTO `kota` VALUES (119, 'Kota Surabaya');

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa`  (
  `id_mahasiswa` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mahasiswa` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nim_mahasiswa` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_kelamin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hobi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kelas` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_kota` int(11) NULL DEFAULT NULL,
  `tgl_input` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_mahasiswa`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
INSERT INTO `mahasiswa` VALUES (1, 'Ramadhan Wahyu', 'M3118073', 'Laki - Laki', 'memancing,olahraga', 'TI C', '0000-00-00', '202cb962ac59075b964b07152d234b70', 'Kauman, Ngasem, Colomadu', 75, '2020-05-04');

SET FOREIGN_KEY_CHECKS = 1;
